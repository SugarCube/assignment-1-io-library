section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi + rax], 0
	je .done
	inc rax
	jmp .loop
.done:
	ret
		

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push rdi
  call string_length
  mov rdx, rax
  mov rax, 1
  pop rsi
  mov rdi, 1
  syscall
  ret
; Принимает код символа и выводит его в stdout
print_char:
  push di
  mov rax, 1
  mov rdi, 1
  mov rdx, 1
  mov rsi, rsp
  syscall
  pop di
  ret
; Принимает код символа и выводит его в stdout
print_newline:
  mov rdi, 0xA
  jmp print_char
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
 mov rsi, 10
  mov rax, rdi
  mov rdi, rsp
  dec rdi
  sub rsp, 24
  mov byte[rdi], 0
  .loop:
  xor rdx, rdx
  div rsi
  or dl, 48
  dec rdi
  mov [rdi], dl
  test rax, rax
  jnz .loop
  call print_string
  add rsp, 24
  ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
  test rdi, rdi
  jns .positive
.minus:
  push rdi
  mov rdi, '-'
  call print_char
  pop rdi
  neg rdi
.positive:
  jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
  xor rax,rax
.loop:
  mov al, byte[rdi]
  cmp al, byte[rsi]
  jne .not_equals
  inc rdi
  inc rsi
  test al, al
  jne .loop
  mov rax, 1
  ret
.not_equals:
  xor rax, rax
  ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  push 0
  mov rax, 0
  mov rdi, 0
  mov rdx, 1 
  mov rsi, rsp
  syscall
  test rax, rax
  js .end
  pop rax
.end:
  ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
  push r12
  push r13
  push r14
  mov r12, rdi
  mov r13, rsi
  xor r14, r14
.skip_loop:
  call read_char
  cmp al, ' '
  je .skip_loop
  cmp al, `\t`
  je .skip_loop
  cmp al, `\n`
  je .skip_loop
.null:
  test r13, r13
  jz .ret_error
.loop:
  test rax, rax
  jz .end
  cmp r14, r13
  jae .long
  mov byte[r12+r14], al
  inc r14
  call read_char
  cmp al, ' '
  je .end
  cmp al, `\t`
  je .end
  cmp al, `\n`
  je .end
  jmp .loop
.long:
  xor rax, rax
  jmp .ret
.end:
  mov byte[r12+r14], 0
.ret_error:
  mov rdx, r14
  mov rax, r12
.ret:
  pop r14
  pop r13
  pop r12
  ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor rdx, rdx
  xor rax, rax
  xor r11, r11
  mov r9, 10
  xor r8, r8
.loop:
  mov r11b, byte [rdi + r8] 
  test r11b, r11b 
  je .end
  cmp r11b, '0' 
  jb .end
  cmp r11b, '9' 
  ja .end
  sub r11b, '0'
  mul r9 
  add rax, r11 
  inc r8
  cmp r8, 20
  je .end
  jmp .loop
.end:
  mov rdx, r8
  ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Копирует строку в буфер
parse_int:
  xor rax, rax
  cmp byte[rdi], '-'
  je .negative
  cmp byte[rdi], '+'
  je .positive
  jmp parse_uint
  .positive:
  inc rdi
  push rdi
  call parse_uint
  pop rdi
  inc rdx
  ret
.negative:
  inc rdi
  push rdi
  call parse_uint
  pop rdi
  neg rax
  inc rdx
  ret
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor rax, rax
  push rbx
.copy_loop:
  cmp rax, rdx
  jg .buffer_full
  mov bl, byte[rdi + rax]
  cmp bl, '0'
  je .end_copy
  mov byte[rsi + rax], bl
  inc rax
  jmp .copy_loop
.buffer_full:
  xor rax, rax
  jmp .end_copy
.end_copy:
  pop rbx
  ret


